function troca(v, i, j)
    aux = v[i]
    v[i] = v[j]
    v[j] = aux
end

function insercao(v)
    tam = length(v)
    for i in 2:tam 
        j = i
        while j > 1
            if compareByValueAndSuit(v[j], v[j - 1])
                troca(v, j, j - 1)
            else
                break
            end
        j = j - 1
        end
    end
    return v
end

function getFigureValue(str)
    val = 1
    fig = str[1]
    if fig == '1'
        val *= 10
    elseif fig == 'J'
        val *= 11
    elseif fig == 'Q'
        val *= 12
    elseif fig == 'K'
        val *= 13
    elseif fig == 'A'
        val *= 14
    else 
        val *= parse(Int64, str[1])
    end
    return val
end

function getSuitValue(str)
    suit = last(str) 
    if suit == '♡' || suit == '♥'
        val = 300
    elseif suit == '♣' || suit == '♧'
        val = 400
    elseif suit == '♢' || suit == '♦'
        val = 100
    elseif suit == '♠' || suit == '♤'
        val = 200
    end
    return val
end

function getCardValue(str)
    val = getFigureValue(str) + getSuitValue(str)
    return val
end

function compareByValue(x, y)
    firstVal = getFigureValue(x)
    secondVal = getFigureValue(y)
    if firstVal < secondVal
        return true
    else 
        return false
    end
end

function compareByValueAndSuit(x, y)
    firstVal = getCardValue(x)
    secondVal = getCardValue(y)
    if firstVal < secondVal
        return true
    else 
        return false
    end
end

