using Test
include("miniep8.jl")

function testInsercao()
    @test insercao(["10♥", "10♦", "K♠", "A♠", "J♠", "A♠"]) == [ "10♦", "J♠", "K♠", "A♠", "A♠", "10♥"]
    @test insercao(["10♡"]) == ["10♡"]
    @test insercao(["A♡", "J♡", "K♣"]) == ["J♡","A♡","K♣"]
    println("The function insercao is working")
end

function testTroca()
    v = [1, 2, 3, 4, 5]
    troca(v, 1, 5)
    @test v == [5, 2, 3, 4, 1]
    troca(v, 1, 1)
    @test v == [5, 2, 3, 4, 1]
    troca(v, 2, 3)
    @test v == [5, 3, 2, 4, 1]
    println("The function troca is working")
end

function testCompareByValue()
    @test compareByValue("2♠", "A♠")
    @test !compareByValue("K♥", "10♥")
    @test !compareByValue("10♠", "10♥")
    println("The function compareByValue is working.")
end

function testGetFigureValue()
    @test getFigureValue("2♠")  == 2
    @test getFigureValue("K♥") == 13
    @test getFigureValue("Q♡") == 12
    @test getFigureValue("10♠") == 10
    println("The function getFigureValue is working.")
end
   
function testGetCardValue()
    @test getCardValue("2♡") == 302
    @test getCardValue("A♠") == 214 
    @test getCardValue("10♢") == 110 
    @test getCardValue("J♣") == 411 
    println("The function getCardValue is working.")
end

function testCompareByValueAndSuit()
    @test compareByValueAndSuit("2♠", "A♠")
    @test !compareByValueAndSuit("K♡", "10♡")
    @test compareByValueAndSuit("10♠", "10♡")
    @test compareByValueAndSuit("A♠", "2♡")
    println("The function compareByValueAndSuit is working.")
end
    
testInsercao()
testTroca()
testGetFigureValue()
testCompareByValue()
testGetCardValue()
testCompareByValueAndSuit()